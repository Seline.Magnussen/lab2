package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    int maxSize = 20;
    List<FridgeItem> foodList = new ArrayList<>();


    @Override
    public int nItemsInFridge() {
        return foodList.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (foodList.size() < maxSize) {
            foodList.add(item);
            return true;
        } else {
        }   return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (foodList.contains(item)) {
            foodList.remove(item);
        }   else {
            throw new NoSuchElementException();
        }
    }
    @Override
    public void emptyFridge() {
        if (foodList.size() > 0) {
            foodList.clear();
        }
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem i : foodList) {
            if (i.hasExpired()) {
                expiredFood.add(i);
            }
        }
        for (FridgeItem e : expiredFood) {
                foodList.remove(e);
            }
        return expiredFood;
        }



}
